# A Simple Sample for ProtoPie iOS Engine SDK

This app runs a bundled pie file with size of 160 x 320 in the center.

You can use the code to learn ProtoPie iOS Engine SDK quickly.

Look into the following files:
- Podfile
- ViewController.swift 
- Main.storyboard
- sample.pie

## How to run
Type the script bellow:
```
pod repo update
pod install
```

Open the file `ProtoPieEngineSimpleSample.xcworkspace` with Xcode.
