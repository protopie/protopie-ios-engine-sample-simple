//
//  Copyright © 2017 Studio XID. All rights reserved.
//

import UIKit
import ProtoPieEngine

fileprivate let PROTOPIE_LICENSE = "KyuSGY6KttPT4h2U8rrvMI4FR5IdBcFabVDJNppcQhYJbaV3qNy+jwuvy2JQcnve+MRsLQEGFwAM1BnugyOrqRX1IDm8qiB4plthJOr7cj9NtB7WDpU0FSG0T1TQVOfz3rQ8mNUy52xA7gKt602jLGMJTylgllnstPOVc1xX7UZEAUyOyox04jgRiPbRntE60g=="

class ViewController: UIViewController {

    @IBOutlet weak var sceneView: PPSceneView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try PPLocalPieManager.shared.applyLicense(licenseStr: PROTOPIE_LICENSE)
        } catch {
            fatalError("Failed to ProtoPie Manager: \(error)")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        runBundeledPie()
    }
    
    private func runBundeledPie() {
        if let samplePiePath = Bundle.main.path(forResource: "sample", ofType: "pie") {
            do {
                if let lpd = try addPie(url: URL(fileURLWithPath: samplePiePath)) {
                    let pie = try PPLocalPieManager.shared.load(id: lpd.id)
                    pie.model.applyScaleFactor(width: Float(sceneView.frame.width), height: Float(sceneView.frame.height))
                    sceneView.startPie(pie: pie)
                }
            } catch {
                print("Failed to load Pie: \(error)")
            }
        } else {
            print("Failed to find the bundled sample pie")
        }
    }
    
    // url 에 저장되어 있는 pie 파일을 local 에 추가합니다.
    private func addPie(url: URL) throws -> PPLocalPieDescriptor? {
        if let lpd = PPLocalPieManager.shared.pie(withExtraKey: "url", withExtraValue: url.absoluteString) {
            // 특정 extra key/value 로 저장된 항목이 있으면, 그것을 사용합니다.
            return lpd
        } else {
            // 저장된 적이 없기 때문에, key/value 값과 함께 새로 추가합니다.
            let extra: [String: String] = [ "url": url.absoluteString ]
            return try PPLocalPieManager.shared.add(url: url, extra: extra)
        }
    }
}
